var Obj = {
    a: 'hello',
    b: 'desde',
    c: 'JavaScript!',
}
var array = Object.values(Obj);

console.log(array);

//El método Object.values toma todos los valores del objeto (no las claves) y los coloca dentro de un arreglo, este se manda a imprimir en consola y listo

//Nos imprime esto: ['hello', 'desde', 'JavaScript!']