//Dado el siguiente código:
class MyClass {
    constructor() {
        this.names_ = [];
    }
    set name(value) {
        this.names_.push(value);
    }
    get name() {
        return this.names_[this.names_.length - 1];
    }
}

const myClassInstance = new MyClass();
myClassInstance.name = 'Joe';
myClassInstance.name = 'Bob';

//Cual es el resultado de ejecuta las siguientes sentencias y porque? :

console.log(myClassInstance.name);
//El resultado es Bob
//Solo devuelve el ultimo valor asignado
console.log(myClassInstance.names_);
//El resultado es ['Joe','Bob']
//Devuelve esto por que se realizo un push el cual agrego un nombre