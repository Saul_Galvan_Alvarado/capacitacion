function foo() {
    var a = 'hello';

    function bar() {
        var b = 'world';
        console.log(a); //Si lo imprimiria
        console.log(b); //Si lo imprimiria
    }
    console.log(a); //Si lo imprimiria
    console.log(b); //No lo imprimiria
}
console.log(a); //Imprime que no esta definido
console.log(b); //Imprime que no esta definido

//Esto se debe a que es una función de alcance por lo que:
//Todo lo definido dentro de la función no es accesible por código fuera de la función. Solo el código dentro de este alcance puede ver las entidades definidas dentro del alcance.