var foo = function(val) {
    return val || 'default';
}

console.log(foo('burguer')); //Devuelve burguer por que se le asigna  como valor esa palabra
console.log(foo(100)); //Devuelve 100 por que se le asigna  como valor es numero
console.log(foo([])); //Devuelve [] por que se le asigna  como valor ese arreglo vacio
console.log(foo(0)); //Devuelve default ya qye en la funcion si no tiene un valor nos manda esa palabra y 0 no lo toma como valor aprovado
console.log(foo(undefined)); //Devuelve default ya qye en la funcion si no tiene un valor nos manda esa palabra y undefind no es un valor