var array = ['a', 'b', 'c', 'd', 'e', 'f'];
array.copyWithin(5, 0, 1); //.copyWithin transfiere una copia  plana de una sección a otra dentro del mismo array ( o contexto similar ), sin modificar su propiedad length y lo devuelve.
array.copyWithin(3, 0, 3);
array.fill('z', 0, 5); //.fill cambia todos los elementos en un arreglo por un valor estático, desde el índice start (por defecto 0) hasta el índice end (por defecto array.length). Devuelve el arreglo modificado.

console.log(array.copyWithin(5, 0, 1));
//devuelve lo mismo
console.log(array.copyWithin(3, 0, 3));
//Devuelve los primeros 3 valores y los repite
console.log(array.fill('z', 0, 5));
//devuelve puros z

var array = ['Alberto', 'Ana', 'Mauricio', 'Bernardo', 'Zoe'];
array.sort(); //.sort ordena los elementos de un arreglo (array) localmente y devuelve el arreglo ordenado. La ordenación no es necesariamente estable
array.reverse(); //.reverse  invierte el orden de los elementos de un array in place. El primer elemento pasa a ser el último y el último pasa a ser el primero.

console.log(array.sort());
//['Alberto', 'Ana', 'Bernardo', 'Mauricio', 'Zoe']
console.log(array.reverse());
//['Zoe', 'Mauricio', 'Bernardo', 'Ana', 'Alberto']