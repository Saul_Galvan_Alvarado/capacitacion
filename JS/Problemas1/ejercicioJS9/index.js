var animal = 'Lion';
switch (animal) {
    case 'Dog':
        console.log('I will not run since animal !== "Dog"');
        break;
    case 'Cat':
        console.log('I will not run since animal !== "Cat"');
        break;
    default:
        console.log('I will run since animal does not match any other case');
}

//Debido a que la variable animal es lion y en ninguno de ampos casos dentro del switch 
//no cuenta con el valor de la variable animal, nos manda como respuesta el default