var array = [5, 10, 15, 20, 25];
Array.isArray(array); //Array.isArray determina si el valor pasado es un Array--por eso manda true
array.includes(10); //.includes determina si una matriz incluye un determinado elemento, devuelve true o false según corresponda--por eso nos manda true
array.includes(10, 2); //Igual que al anterior solo que nos manda un false por que nuestro arrego no contiene 2
array.indexOf(25); //.indexOf retorna el primer índice en el que se puede encontrar un elemento dado en el array, ó retorna -1 si el elemento no esta presente.
array.lastIndexOf(10, 0); // .lastIndexOf devuelve el último índice en el que un cierto elemento puede encontrarse en el arreglo, ó -1 si el elemento no se encontrara. El arreglo es recorrido en sentido contrario, empezando por el índice fromIndex.

console.log(Array.isArray(array)); //true
console.log(array.includes(10)); //true
console.log(array.includes(10, 2)); //false
console.log(array.indexOf(25)); //4
console.log(array.lastIndexOf(10, 0)); //-1