var animal = 'kitty';
var result = (animal === 'kitty') ? 'cute' : 'still nice';
console.log(result)

//Nos muestra 'cute', Por que si no fuera kitty la respuesta seria 'still nice'
//Esto es debido a que se hace la comparaciòn y como son iguales nos manda cute, si no fueran iguales nos mandaria still nice