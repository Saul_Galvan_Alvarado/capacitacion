var a = [1, 2, 3, 8, 9, 10];
a.slice(0, 3).concat([4, 5, 6, 7], a.slice(3, 6));
console.log(a);
//Nos devuelve [1, 2, 3, 8, 9, 10]
var a = [1, 2, 3, 8, 9, 10];
a.splice(3, 0, ...[4, 5, 6, 7, ]);
console.log(a); //Nos devuelve [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

//.slice devuelve una copia de una parte del array dentro de un nuevo array empezando por inicio hasta fin (fin no incluido). El array original no se modificará.

//.concat se usa para unir dos o más arrays. Este método no cambia los arrays existentes, sino que devuelve un nuevo array.

//.splice cambia el contenido de un array eliminando elementos existentes y/o agregando nuevos elementos.