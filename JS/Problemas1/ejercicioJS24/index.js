var array = ['a', 'b', 'c'];
array.join('->');
console.log(array); //nos imprime ['a', 'b', 'c']
array.join('.');
console.log(array); //nos imprime ['a', 'b', 'c']
'a', 'b', 'c'.split('.');
console.log(array); //nos imprime ['a', 'b', 'c']
'5.4.3.2.1'.split('.');
console.log(array); //nos imprime ['a', 'b', 'c']

//.join une todos los elementos de una matriz (o un objeto similar a una matriz) en una cadena y devuelve esta cadena.
//.split ambia el contenido de un array eliminando elementos existentes y/o agregando nuevos elementos.