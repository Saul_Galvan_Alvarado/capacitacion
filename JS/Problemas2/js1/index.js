var personArr = [{
        "personaId": 123,
        "name": "Jhon",
        "city": "Melbourne",
        "phoneNo": "1234567890"
    },
    {
        "personaId": 124,
        "name": "Amelia",
        "city": "Sydney",
        "phoneNo": "1234567890"
    },
    {
        "personaId": 125,
        "name": "Emily",
        "city": "Perth",
        "phoneNo": "1234567890"
    },
    {
        "personaId": 126,
        "name": "Abraham",
        "city": "Perth",
        "phoneNo": "1234567890"
    }
];

console.table(personArr);

let crearTabla = function(lista) {
    let stringTabla = "<tr><th>personaId</th><th>name</th><th>city</th><th>phoneNo</th></tr>";
    for (let tarea of lista) {
        let fila = "<tr> <td>"
        fila += tarea.personaId;
        fila += "</td>"

        fila += "<td>"
        fila += tarea.name;
        fila += "</td>"

        fila += "<td>"
        fila += tarea.city;
        fila += "</td>"

        fila += "<td>"
        fila += tarea.phoneNo;
        fila += "</td>"

        fila += "</tr>";
        stringTabla += fila;
    }
    return stringTabla;
};

document.getElementById("tablaPersonas").innerHTML =
    crearTabla(personArr);