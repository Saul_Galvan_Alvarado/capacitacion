function divide(dividendo, divisor) {
    return new Promise((resolve, reject) => {
        if (divisor === 0) {
            reject(new Error('No se puede dividir entre 0'));
        } else {
            resolve(dividendo / divisor);
        }
    });
}
//esperamos por la respuesta
try {
    const result = divide(5, 1);
    console.log(result);
} catch (err) {
    console.error(err.message);
}