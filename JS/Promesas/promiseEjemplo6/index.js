//Encadenamiento
new Promise(function(resolver, rechazar) {
        console.log('Inicial');
        resolver();
    })
    .then(() => {
        //Despues de throw ya no se ejeuta la linea de abajo de el
        throw new Error('Algo fallo');
        console.log('Haz esto');
    })
    .catch(() => {
        console.log('Haz eso');
    })
    .then(() => {
        console.log('Haz esto sin que importe lo que sucedio antes');
    });