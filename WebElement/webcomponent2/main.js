class MiBotonExtendido extends HTMLButtonElement {
    constructor() {
        super();
        this.addEventListener("click", (e) => {
            console.log("evento click: " + this.innerHTML);
            alert("MiBotonExtendido");
        });
    }
    static get ceName() {
        return "mi-boton-extedido";
    }
    get is() {
        return this.getAttribute("is");
    }
    set is(value) {
        this.setAttribute("is", value || this.ceName);
    }
}

customElements.define("mi-boton-extedido", MiBotonExtendido, {
    extends: "button",
});


//Puedes crear un nuevo elemento usando la API del DOM
const miBotonExtendido2 = document.createElement('button', { is: MiBotonExtendido.ceName });

miBotonExtendido2.textContent = "Soy un mi-boton-extendido";
document.body.appendChild(miBotonExtendido2);

const miBotonExtendido3 = document.createElement('button', { is: MiBotonExtendido.ceName });
miBotonExtendido3.textContent = "Hola-btn3";
document.querySelector('#container').appendChild(miBotonExtendido3);