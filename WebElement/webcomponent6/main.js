class MiSaludo extends HTMLElement {
    constructor() {
        //Obtengo la única etiqueta 'template'
        const tpl = document.querySelector('template');
        //Clono su contenido y se crea una instancia del documento fragment
        const tplInst = tpl.content.cloneNode(true);

        super(); //Invoca el constructor de la clase padre
        //Se crea un shadow dom para las instancias de mi-saludo
        this.attachShadow({ mode: 'open' });
        //Y se agrega el template dentro del shadow Dom usando el elemento ríz 'shadowRoot'
        this.shadowRoot.appendChild(tplInst);
    }
}

//Se registra el custom element para poder ser utilizado declarativamente en el HTML o imperactivamente mediante JS
customElements.define('mi-saludo', MiSaludo);