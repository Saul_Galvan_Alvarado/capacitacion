obtenerEmpleados();

async function obtenerEmpleados() {
    try {
        const url = "http://dummy.restapiexample.com/api/v1/employees";
        const response = await fetch(url);
        console.log(response);
        console.log("");
        const datos = await response.json();
        let empleados = datos.data;
        console.log(empleados);
        let body = "";
        for (var i = 0; i < empleados.length; i++) {
            body += `<tr><td>${empleados[i].id}</td><td>${empleados[i].employee_name}</td><td>${empleados[i].employee_salary}</td></tr>`;
        }
        document.getElementById("empleados").innerHTML = body;
    } catch (error) {
        console.log(error);
    }
}

async function buscarEmpleados(id) {
    try {
        const url = "http://dummy.restapiexample.com/api/v1/employees";
        const response = await fetch(url);
        const datos = await response.json();
        let empleados = datos.data;
        for (let obj of empleados) {
            if (obj.id == id) {
                let contenido = document.getElementById("empleados");
                contenido.textContent = "";
                let horizontal = document.createElement("tr");
                for (let key in obj) {
                    let columna = document.createElement("td");
                    let texto = document.createTextNode(obj[key]);
                    columna.appendChild(texto);
                    horizontal.appendChild(columna);
                }
                contenido.appendChild(horizontal);
                return;
            }
        }
    } catch (error) {
        console.log(error);
    }
}

class MiBotonExtendidoBuscar extends HTMLButtonElement {
    constructor() {
        super();
        this.addEventListener("click", (e) => {
            let info = document.querySelector("#buscar");
            let input = info.value;
            buscarEmpleados(input.trim());
        });
    }
    static get ceName() {
        return "mi-boton-buscar";
    }
    get is() {
        return this.getAttribute("is");
    }
    set is(value) {
        this.setAttribute("is", value || this.ceName);
    }
}

class MiBotonExtendidoLimpiar extends HTMLButtonElement {
    constructor() {
        super();
        this.addEventListener("click", (e) => {
            document.getElementById("buscar").value = "";
            obtenerEmpleados();
        });
    }
    static get ceName() {
        return "mi-boton-limpiar";
    }
    get is() {
        return this.getAttribute("is");
    }
    set is(value) {
        this.setAttribute("is", value || this.ceName);
    }
}
customElements.define("mi-boton-buscar", MiBotonExtendidoBuscar, {
    extends: "button",
});
customElements.define("mi-boton-limpiar", MiBotonExtendidoLimpiar, {
    extends: "button",
});