class MiMensaje extends HTMLElement {
    constructor() {
            super();
            this.addEventListener('click', function(e) {
                alert('Click en mensaje, Soy Saúl');
            });
            console.log('constructor: Cuando el elemento es creado');
        }
        //propiedad para escuchar solo los atributos definidos en el arreglo usado
        //atributeChangedCallback
    static get observedAttributes() {
            return ['msj'];
        }
        //Callback cuando se inserta el elemento en el DOM
    connectedCallback() {
        console.log('connectedCallback: Cuando el elemento es insertado en el documento');
    }
    disconnectedCallback() {
        alert('disconnected: Cuando el elemento es eliminado del documento')
    }
    adoptedCallback() {
            alert('adoptedCallback: Cuando el elemento es adoptado por otro documento');
        }
        //Cuando un atributo es modificado, solo llamado en atributos definidos en la propiedad observedAttribute
    attributeChangedCallback(attrName, oldVal, newVal) {
        console.log('attributeChangedCallback: Cuando cambia un atributo');
        if (attrName == 'msj') {
            this.pintarMensaje(newVal);
        }
    }

    //Pintar el mensaje que se declara en el atributo 'msj'
    pintarMensaje(msj) {
            this.innerHTML = msj
        }
        //acceso a los atributos del DOM
        //propiedad 'msj' sincronizada con el atributo 'msk'
    get msj() {
        return this.getAttribute('msj');
    }
    set msj(val) {
            this.setAttribute('msj', val);
        }
        ///////////////////////////
        //propiedad 'casiVisible' sincronizada con el atributo 'casiVisible'
    get casiVisible() {
        return this.hasAttribute('casiVisible');
    }
    set casiVisible(value) {
            if (value) {
                this.setAttribute('casiVisible', '');
            } else {
                this.removeAttribute('casiVisible')
            }
        }
        //define la opacidad del elemento basado en la propiedad casiVisible
    setCasiVisible() {
        if (this.casiVisible) {
            this.style.opacity = 0.1;
        } else {
            this.style.opacity = 1;
        }
    }

    static get observedAttributes() {
        return ['msj', 'casiVisible']
    }

    //Cuando un atributo es modificado, solo llemado en atributos observa la propiedad observedAttributes
}

customElements.define('mi-mensaje', MiMensaje);


//Segundo componente
let miMensaje = document.createElement('mi-mensaje')
miMensaje.msj = 'Otro mensaje';
document.body.appendChild(miMensaje);

//Tambien puedes crear un elemento con el operador new
let tercerMensaje = new MiMensaje();
tercerMensaje.msj = 'Tercer mensaje';
document.body.appendChild(tercerMensaje);