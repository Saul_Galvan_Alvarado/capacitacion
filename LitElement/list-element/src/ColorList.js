// eslint-disable-next-line import/no-extraneous-dependencies
import { html, LitElement } from 'lit-element';
// eslint-disable-next-line import/extensions
import './ListElement';

export class ColorList extends LitElement {
    static get properties() {
        return {
            colors: { type: Array },
        };
    }

    constructor() {
        super();
        this.colors = ['Rojo', 'Verde', 'Amarillo', 'Azul'];
    }

    render() {
        return html `
 <list-element .items="${this.colors}">
 </list-element>
`;
    }
}