import { html, LitElement } from 'lit';

export class TemplateBind extends LitElement {
    static get properties() {
        return {
            prop1: { type: String },
            prop2: { type: String },
            prop3: { type: Boolean },
            prop4: { type: String },
            activo: { type: Boolean },
        };
    }

    constructor() {
        super();
        this.prop1 = 'text binding';
        this.prop2 = 'mydiv';
        this.prop3 = true;
        this.prop4 = 'pie';
        this.activo = true;
    }

    render() {
        return html `
      <div>${this.prop1}</div>
      <div id="${this.prop2}">atribute binding</div>
      <div>
        boolean atribute binding
        <input type="text" ?disabled="${this.prop3}" />
      </div>
      <div>
        property binding
        <input type="text" .value="${this.prop4}" />
      </div>

      <div>
        event handler binding
        <button @click="${this.clickHandler}">Click</button>
      </div>

      <p>
        <input
          type="checkbox"
          ?checked="${this.activo}"
          @change="${this.doChange}"
        />
      </p>
    `;
    }

    // eslint-disable-next-line class-methods-use-this
    clickHandler(e) {
        console.log(e.target);
    }

    doChange(e) {
        console.log('El activo es: ', this.activo);
        this.activo = e.target.checked;
    }
}