import { html, css, LitElement } from 'lit';

export class ShadowTreeStyle extends LitElement {
    static get styles() {
        return css `
      * {color: red;}
      p {font-family: sans-serif;}
      .myclass{margin:100px;}
      #main {padding: 30px;}
      h1 {font-size:4em;};
    `;
    }


    render() {
        return html `
<p> Texto........</p>
<p class="myclass"> Saúl........</p>
<p id="main"> Galván Alvarado........</p>
<h1>Estilos</h1>
    `;
    }
}