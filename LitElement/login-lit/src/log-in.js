import { LitElement, html, css, customElement } from 'lit-element';
export class LogIn extends LitElement {
    static get styles() {
        return css `
      .container {
        border: solid 3px black;
        border-radius: 10px;
        width: 300px;
        height: 300px;
        text-align: center;
      }
      input {
        width: 80%;
        height: 30px;
        margin-top: 5vh;
      }

      button {
        width: 50%;
        height: 30px;
        background: green;
        margin-top: 7vh;
        border: none;
      }
      button:hover {
        background: grey;
      }
    `;
    }
    render() {
        return html `
      <div class="container">
        <h2>Login</h2>
        <input id="user" type="email" placeholder="Ingresa tu usuario" />

        <input id="pass" type="password" placeholder="Ingresa tu contraseña" />

        <button @click="${this._ok}">Ingresar</button>
      </div>
    `;
    }
    _ok() {
        const user = this.shadowRoot.querySelector('#user').value;
        const pass = this.shadowRoot.querySelector('#pass').value;

        if (user == 'admin' && pass == 'admin') {
            this.dispatchEvent(
                new CustomEvent('exito', {
                    detail: { login: true },
                    bubbles: true,
                    composed: true,
                })
            );
        }
    }
}
customElements.define('log-in', LogIn);