import { LitElement, html, css } from 'lit';
export class ApiTable extends LitElement {
    static get properties() {
        return {
            columna_empleado: {
                type: Array,
            },
            values: {
                type: String,
            },
        };
    }

    constructor() {
        super();
        this.columna_empleado = {};
    }

    static get styles() {
        return css `
      .contenedor {
        border: solid 3px black;
        border-radius: 10px;
        width: 700px;
        height: 1050px;
        text-align: center;
      }
      table {
        font-family: 'Lucida Sans Unicode', 'Lucida Grande', Sans-Serif;
        font-size: 12px;
        margin: 45px;
        width: 480px;
        text-align: left;
        border-collapse: collapse;
      }

      th {
        font-size: 13px;
        font-weight: normal;
        padding: 8px;
        background: #b9c9fe;
        border-top: 4px solid #aabcfe;
        border-bottom: 1px solid #fff;
        color: #039;
      }

      td {
        padding: 8px;
        background: #e8edff;
        border-bottom: 1px solid #fff;
        color: #669;
        border-top: 1px solid transparent;
      }

      tr:hover td {
        background: #d0dafd;
        color: #339;
      }

      .btn-bootstrap {
        padding: 0.375rem 0.75rem;
        border-radius: 0.25rem;
        background-color: #007bff;
        color: #fff;
        transition: background-color 0.15s ease-in-out;
        margin-top: 5vh;
      }

      .btn-bootstrap:hover,
      .btn-bootstrap:focus {
        background-color: #0069d9;
      }

      .btn-bootstrap:active {
        background-color: #0062cc;
      }

      #bt3 {
        border: 1px solid #000000;
        background: orange;
        color: black;
        padding: 0.375rem 0.75rem;
        border-radius: 0.25rem;
      }
      #bt4 {
        border: 1px solid #000000;
        background: red;
        color: black;
        padding: 0.375rem 0.75rem;
        border-radius: 0.25rem;
      }
      .content_buscar {
        margin-top: 5vh;
        text-align: center;
      }
      .btn {
        border: 1px solid #000000;
        background: yellow;
        color: black;
        padding: 0.375rem 0.75rem;
        border-radius: 0.25rem;

        input {
          width: 80%;
          height: 30px;
        }
      }
    `;
    }
    render() {
            return html `
      <div class="contenedor">
        <button
          @click=${this.obtenerEmpleados}
          class="btn-bootstrap"
          id="btn-bootstrap"
        >
          Muestra los Empleados
        </button>
        <div class="content_buscar">
          <label >ID:</label>
          <input
            name="buscarEmpleados"
            id="buscarEmpleados"
            placeholder="Ingrese id del empleado"
          />

          <button @click="${this.buscarEmpleados}" class="btn">Buscar</button>
          <button @click="${this.limpiar}" class="btn">Limpiar</button>
        </div>
        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Nombre</th>
              <th>Salario</th>
              <th>Edad</th>
            </tr>
          </thead>
          <tbody>
            ${Object.keys(this.columna_empleado).map(
              item => html`
                <tr></tr>
                ${Object.values(this.columna_empleado[item]).map(
                  val => html`<td>${val}</td>`
                )}
              `
            )}
          </tbody>
          <tfoot></tfoot>
        </table>
        <button type="button" id="bt3">Ok</button>
        <button type="button" id="bt4">Cancel</button>
      </div>
    `;
  }
  async obtenerEmpleados() {
    const url = 'http://dummy.restapiexample.com/api/v1/employees';
    const response = await fetch(url);
    const datos = await response.json();
    let empleados = datos.data;
    console.log(empleados);
    this.columna_empleado = empleados;
  }
  async buscarEmpleados() {
    const url = 'http://dummy.restapiexample.com/api/v1/employees';
    const response = await fetch(url);
    const datos = await response.json();
    let empleados = datos.data;
    const busqueda = this.shadowRoot.querySelector('#buscarEmpleados').value;
    const result = empleados.filter(item => item.employee_name === busqueda);

    console.log(result);
    this.columna_empleado = result;
  }
  async limpiar(){
    let info = this.shadowRoot.querySelector("#buscar");
    let input = info.value;
    buscarEmpleados(input.trim());
  }
}
customElements.define('api-table', ApiTable);