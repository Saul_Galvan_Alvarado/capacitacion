import { LitElement, html, css } from 'lit';
import './log-in.js';
import './api-table.js';
export class LoginLit extends LitElement {
    static get properties() {
        return {
            ingreso: {
                type: Boolean,
            },
        };
    }

    static get styles() {
        return css `
      log-in {
        position: absolute;
        top: 10%;
        display: flex;
        left: 40%;
        right: 40%;
      }
      api-table {
        position: absolute;
        display: flex;
        left: 20%;
        right: 20%;
      }
    `;
    }

    constructor() {
        super();
    }

    render() {
            return html `
      ${this.ingreso
        ? html` <api-table></api-table> `
        : html` <log-in @exito="${this._cambioLogin}"></log-in> `}
    `;
  }
  _cambioLogin() {
    this.ingreso = true;
  }
}