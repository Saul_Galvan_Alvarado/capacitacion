import { html, css, LitElement } from 'lit';

export class MyAccesors extends LitElement {
    static get styles() {
        return css `
      :host {
        display: block;
        padding: 25px;
        color: var(--my-accesors-text-color, #000);
      }
    `;
    }

    static get properties() {
        return {
            prop: { type: Number },
        };
    }

    set prop(val) {
        let oldVal = this._prop;
        this._prop = Math.floor(val);
        this.requestUpdate('prop', oldVal);
    }

    get prop() {
        return this._prop;
    }

    constructor() {
        super();
        this._prop = 0;
    }

    render() {
        return html `
      <p>prop: ${this.prop}</p>
      <button @click="${() => {this.prop = Math.random()*10;}}">change prop</button>
    `;
    }
}