//HTMl - Tagged template String
//Render - Volcar el html generado con los templates strings
import { html, render } from "./node_modules/lit-html/lit-html.js";

//Render recibe dos parametros 
//templateResult : el resultado de obtener y generar un template.
//nodo dom : donde volvar el html generado
const cadena = ">>>>>Contenido dinamico<<<<<";
/* Version 1
const objectTemplateResult = html <h2>Contenido estático (template) + ${cadena} </h2>;
*/
//Version 2 -> function dinamica (parametro)
const templateHolder = (paramString) => html `<h2>Contenido estático (template) + ${paramString} </h2>`;
const objectTemplateResult = templateHolder(cadena);

//render -> sustituye todo lo del body
//render(objectTemplateResult, elementoEnDom);
render(templateHolder(cadena), document.getElementById('container1'));
render(templateHolder('Hola, me llamo Saúl'), document.getElementById('container2'));