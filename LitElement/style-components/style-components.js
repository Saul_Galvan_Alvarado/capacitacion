import { StyleComponents } from './src/StyleComponents.js';
import { HerenciaElemento } from './src/HerenciaElemento.js';
import { SuperElemento } from './src/SuperElemento.js';


window.customElements.define('style-components', StyleComponents);
window.customElements.define('herencia-elemento', HerenciaElemento);
window.customElements.define('super-elemento', SuperElemento);